﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5092_A1
{
    class Summary
    {
        public void Sum()
        {
            double[,] R1 = new double[IO.N, IO.steps + 1];
            R1 = RandGenerator.Randn(IO.N, IO.steps, IO.Thread);
            EuroOption O1 = new EuroOption();
            O1.S0 = IO.S0;
            O1.K = IO.K;
            O1.sigma = IO.sigma;
            O1.r = IO.r;
            O1.T = IO.T;
            O1.isput = IO.isput;
            O1.R1 = R1;

            IO.Price = Math.Truncate(O1.PriceEuro(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
            Program.increase(1);

            IO.Delta = Math.Truncate(GreeksCalculator.Delta(O1.isput, O1.K, O1.S0, O1.sigma, O1.r, O1.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
            Program.increase(1);

            IO.Gamma = Math.Truncate(GreeksCalculator.Gamma(O1.isput, O1.K, O1.S0, O1.sigma, O1.r, O1.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
            Program.increase(1);

            IO.Vega = Math.Truncate(GreeksCalculator.Vega(O1.isput, O1.K, O1.S0, O1.sigma, O1.r, O1.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
            Program.increase(1);

            IO.Theta = Math.Truncate(GreeksCalculator.Theta(O1.isput, O1.K, O1.S0, O1.sigma, O1.r, O1.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
            Program.increase(1);

            IO.Rho = Math.Truncate(GreeksCalculator.Rho(O1.isput, O1.K, O1.S0, O1.sigma, O1.r, O1.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
            Program.increase(1);

            IO.SE = Math.Truncate(O1.Std(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT)*100000)/100000;
            Program.increase(1);
        }
    }
}
