﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5092_A1
{
    class IO
    {
        public static int N { get; set; }
        public static int steps { get; set; }
        public static Boolean IsAnti { get; set; }
        public static Boolean IsCV { get; set; }
        public static Boolean IsMT { get; set; }
        public static int Thread { get; set; }
        public static string Time { get; set; }

        public static Boolean isput { get; set; }
        public static double S0 { get; set; }
        public static double K { get; set; }
        public static double sigma { get; set; }
        public static double r { get; set; }
        public static double T { get; set; }

        public static double Price { get; set; }
        public static double Delta { get; set; }
        public static double Gamma { get; set; }
        public static double Vega { get; set; }
        public static double Theta { get; set; }
        public static double Rho { get; set; }
        public static double SE { get; set; }

    }
}
