﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

//Jinyuan Zhang, the first assignment of FM 5092.
namespace FM5092_A1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            errorProvider1.SetIconAlignment(txtS, ErrorIconAlignment.MiddleRight);
            errorProvider1.SetIconAlignment(txtK, ErrorIconAlignment.MiddleRight);
            errorProvider1.SetIconAlignment(txtsigma, ErrorIconAlignment.MiddleRight);
            errorProvider1.SetIconAlignment(txtr, ErrorIconAlignment.MiddleRight);
            errorProvider1.SetIconAlignment(txtT, ErrorIconAlignment.MiddleRight);
            errorProvider1.SetIconAlignment(txtsteps, ErrorIconAlignment.MiddleRight);
            errorProvider1.SetIconAlignment(txtN, ErrorIconAlignment.MiddleRight);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            IO.S0 = Convert.ToDouble(txtS.Text);
            IO.K = Convert.ToDouble(txtK.Text);
            IO.sigma = Convert.ToDouble(txtsigma.Text);
            IO.r = Convert.ToDouble(txtr.Text);
            IO.T = Convert.ToDouble(txtT.Text);
            if (radioCall.Checked == true)
            {
                IO.isput = false;
            }
            else if (radioPut.Checked == true)
            {
                IO.isput = true;
            }

            if (checkAnti.Checked == false)
            {
                IO.IsAnti = false;
            }
            else if (checkAnti.Checked == true)
            {
                IO.IsAnti = true;
            }

            if (checkCV.Checked==false)
            {
                IO.IsCV = false;
            }
            else
            {
                IO.IsCV = true;
            }
            
            if (checkMT.Checked == false)
            {
                IO.IsMT = false;
                IO.Thread = 1;
            }
            else
            {
                IO.IsMT = true;
                IO.Thread = System.Environment.ProcessorCount;
            }
            
            IO.N = Convert.ToInt32(txtN.Text);
            IO.steps = Convert.ToInt32(txtsteps.Text);
            

            Options opt = new Options();

            Thread C = new Thread(new ThreadStart(opt.scheduler));
            C.Start();

        }

        public void finish()
        {
            lblPrice.Text = Convert.ToString(IO.Price);
            lblDelta.Text = Convert.ToString(IO.Delta);
            lblGamma.Text = Convert.ToString(IO.Gamma);
            lblRho.Text = Convert.ToString(IO.Rho);
            lblTheta.Text = Convert.ToString(IO.Theta);
            lblVega.Text = Convert.ToString(IO.Vega);
            lblStd.Text = Convert.ToString(IO.SE);
            lblCore.Text = Convert.ToString(System.Environment.ProcessorCount);
            lblTime.Text = IO.Time;
        }

        //In the following section, we used an error provider to handle error inputs.
        //If an user entered a value that is not a number, he will be warned.
        private void txtS_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(txtS.Text, out num))
                errorProvider1.SetError(txtS, "please enter a number");
            else
                errorProvider1.SetError(txtS, string.Empty);
        }

        private void txtK_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(txtK.Text, out num))
                errorProvider1.SetError(txtK, "please enter a number");
            else
                errorProvider1.SetError(txtK, string.Empty);
        }

        private void txtsigma_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(txtsigma.Text, out num))
                errorProvider1.SetError(txtsigma, "please enter a number");
            else
                errorProvider1.SetError(txtsigma, string.Empty);
        }

        private void txtr_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(txtr.Text, out num))
                errorProvider1.SetError(txtr, "please enter a number");
            else
                errorProvider1.SetError(txtr, string.Empty);
        }

        private void txtT_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(txtT.Text, out num))
                errorProvider1.SetError(txtT, "please enter a number");
            else
                errorProvider1.SetError(txtT, string.Empty);
        }

        private void txtsteps_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(txtsteps.Text, out num))
                errorProvider1.SetError(txtsteps, "please enter a number");
            else
                errorProvider1.SetError(txtsteps, string.Empty);
        }

        private void txtN_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(txtN.Text, out num))
                errorProvider1.SetError(txtN, "please enter a number");
            else
                errorProvider1.SetError(txtN, string.Empty);
        }
       
    }
}
